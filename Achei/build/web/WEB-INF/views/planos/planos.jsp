<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>
        <link rel="stylesheet" href="achei.css">
        <!-- Bootstrap -->
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<c:url value="/resources/js/html5shiv.min.js"/>"></script>
          <script src="<c:url value="/resources/js/respond.min.js"/>"></script>
        <![endif]-->
    </head>
    <style>
        h1{
            color: orangered;
            font-family: verdana;
            font-size: 50px;

        }
        p{
            color: black;
            font-family: verdana;
            font-size: 10px;
        }
        h2{
            color: orange;
            font-family: verdana;
            font-size: 30px;
        }



    </style>
    <body>
        <form method="post">
            <br>   
            <div class="row">
                <div class=" col-xs-2" style=" alignment-adjust: 10px;margin-left: 10px; ">
                </div>
                <div class=" col-xs-12" >
                    <div class="panel panel-default" style="margin-left: 20px;margin-right: 20px">
                        <div class="panel-body" style="margin-left: 100px">
                            <jsp:include page="../NavigationBar/navbar.jsp"/> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">

                <br>
                <br>

                <div class="col-md-2"></div>
                <div class="col-md-2">
                    <div class="thumbnail">

                        <div class="caption">
                            <b><label for="gratis" style="font-size: 50px;font-family: sans-serif;color: orangered;margin-left: 12px">Grátis</label></b> 
                            <center><p><label for="gratis" >Disponibilidade de Uso</label></p></center>
                            <center><label for="gratis" style="font-family: sans-serif;font-size: 30px;color: orange">15 Dias</label></center>
                            <center><label for="gratis" style="font-family: sans-serif;font-size: 20px;color: orange">R$00,00</label></center>
                            <input type="radio" id="gratis" name="planos" class="plano" style="margin-left: 72px" value="1"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-0"></div>
                <div class="col-md-2">
                    <div class="thumbnail">
                        <div class="caption">
                            <b><label for="basico" style="font-size: 50px;font-family: sans-serif;color: orangered;margin-left: 12px">Básico</label></b> 
                            <center><p><label for="basico" >Disponibilidade de Uso</label></p></center>
                            <center><label for="basico" style="font-family: sans-serif;font-size: 30px;color: orange">3 Meses</label></center>
                            <center><label for="gratis" style="font-family: sans-serif;font-size: 20px;color: orange">R$10,00</label></center>
                            <input type="radio" id="basico" name="planos" style="margin-left: 72px" value="2"/>

                        </div>
                    </div>
                </div>
                <div class="col-md-offset-0"></div>
                <div class="col-md-2">
                    <div class="thumbnail">
                        <div class="caption">
                            <b><label for="medio" style="font-size: 50px;font-family: sans-serif;color: orangered;margin-left: 12px">Médio</label></b> 
                            <center><p><label for="medio" >Disponibilidade de Uso</label></p></center>
                            <center><label for="medio" style="font-family: sans-serif;font-size: 30px;color: orange">6 Meses</label></center>
                            <center><label for="gratis" style="font-family: sans-serif;font-size: 20px;color: orange">R$20,00</label></center>
                            <input type="radio" id="medio" name="planos" style="margin-left: 72px"value="3">

                        </div>
                    </div>
                </div>
                <div class="col-md-offset-0"></div>
                <div class="col-md-2">
                    <div class="thumbnail">
                        <div class="caption">
                            <b><label for="pro" style="font-size: 50px;font-family: sans-serif;color: orangered;margin-left: 32px">Pro</label></b> 
                            <center><p><label for="pro" >Disponibilidade de Uso</label></p></center>
                            <center><label for="pro" style="font-family: sans-serif;font-size: 30px;color: orange">1 Ano</label></center>
                            <center><label for="gratis" style="font-family: sans-serif;font-size: 20px;color: orange">R$30,00</label></center>
                            <input type="radio" id="pro" name="planos" style="margin-left: 72px" value="4" > 

                        </div>

                    </div>
                </div>

            </div> 

            <div class="row clearfix"></div>
            <div class col col-md-9>
                <a href="/web/profissional/termo" style="margin-left: 55px"><center>Termo de contrato e prestação de serviço</a><br></center>
                <br>
                <center><p><b>Li e concordo com o termo de contrato</p></b></center>
                <div class="col col-offset-0"></div>
                <div class col col-md-3>
                    <center><input type="radio" id = "termoSim" name ="termo" style="margin-left: 10px" value="1"> Aceito</input></center>
                    <center><input type="radio" id = "termoNao" name ="termo" style="margin-left: 10px" value="0"> Não Aceito</input></center>
                        <c:if test="${param.termo == 's'} ">



                    </c:if>
                    <input type="Submit" class=" btn btn-warning" style="margin-left: 640px" value="Confirmar">
                    </form>
                </div>
                <br>

                <!--  <center><label for="ok" class="btn btn-warning" style="margin-left: 50px"><b>Confirmar Cadastro</b></label></center>
                  <p id="demo"> </p> -->
            </div>



            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    </body> 
</html>
